FROM debian:buster-slim


RUN apt-get update && apt-get --assume-yes upgrade

RUN apt-get install --assume-yes \
    apt-utils \
    #
    cowsay

RUN apt-get install --assume-yes \
    python3 \
    python3-pip \
    #
    cowsay

RUN python3 -m pip install --upgrade pip

RUN apt-get install --assume-yes \
    python3-numpy \
    python3-pil \
    ttf-dejavu \
    #
    cowsay

RUN apt-get install --assume-yes apt-utils wget
RUN wget https://archive.raspbian.org/raspbian.public.key -O - | apt-key add -
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 7EA0A9C3F273FCD8
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 82B129927FA3303E


WORKDIR /local
COPY raspberrypi.list /etc/apt/sources.list.d
RUN apt-get update


RUN python3 -m pip install --force-reinstall adafruit-blinka
RUN python3 -m pip install rpi.gpio
RUN python3 -m pip install adafruit-circuitpython-rgb-display

RUN apt-get install --assume-yes libraspberrypi-bin
RUN apt-get install --assume-yes procps

COPY rgb_display_minipitftstats.py .
