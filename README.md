# mini-pitft

Adafruit Mini PiTFT

Docker image for driving Adafruit Mini PiTFT with host statistics

`docker run --privileged --restart=always --detach registry.gitlab.com/mpinet/mini-pitft:20210105 python3 rgb_display_minipitftstats.py`

# Notes

https://www.raspbian.org/RaspbianRepository

https://chrisjean.com/fix-apt-get-update-the-following-signatures-couldnt-be-verified-because-the-public-key-is-not-available/

https://learn.adafruit.com/neopixels-on-raspberry-pi

https://learn.adafruit.com/adafruit-mini-pitft-135x240-color-tft-add-on-for-raspberry-pi/python-stats

https://askubuntu.com/questions/438345/how-to-remove-install-a-package-that-is-not-fully-installed

