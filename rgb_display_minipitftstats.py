# -*- coding: utf-8 -*-

import time
import subprocess
import digitalio
import board
from PIL import Image, ImageDraw, ImageFont
import adafruit_rgb_display.st7789 as st7789


# Configuration for CS and DC pins (these are FeatherWing defaults on M0/M4):
cs_pin = digitalio.DigitalInOut(board.CE0)
dc_pin = digitalio.DigitalInOut(board.D25)
reset_pin = None

# Config for display baudrate (default max is 24mhz):
BAUDRATE = 64000000

# Setup SPI bus using hardware SPI:
spi = board.SPI()

disp = st7789.ST7789(
   spi,
   cs=cs_pin,
   dc=dc_pin,
   rst=reset_pin,
   baudrate=BAUDRATE,
   width=240,
   height=240,
   x_offset=0,
   y_offset=80,
)



# Create blank image for drawing.
# Make sure to create image with mode 'RGB' for full color.
height = disp.width  # we swap height/width to rotate it to landscape!
width = disp.height
image = Image.new("RGB", (width, height))
rotation = 270

# Get drawing object to draw on image.
draw = ImageDraw.Draw(image)

# Draw a black filled box to clear the image.
draw.rectangle((0, 0, width, height), outline=0, fill=(0, 0, 0))
disp.image(image, rotation)
# Draw some shapes.
# First define some constants to allow easy resizing of shapes.
padding = -2
top = padding
bottom = height - padding
# Move left to right keeping track of the current x position for drawing shapes.
x = 0


# Alternatively load a TTF font.  Make sure the .ttf font file is in the
# same directory as the python script!
# Some other nice fonts to try: http://www.dafont.com/bitmap.php
font = ImageFont.truetype("/usr/share/fonts/truetype/dejavu/DejaVuSans.ttf", 18)

# Turn on the backlight
backlight = digitalio.DigitalInOut(board.D22)
backlight.switch_to_output()
backlight.value = True

while True:
    # Draw a black filled box to clear the image.
    draw.rectangle((0, 0, width, height), outline=0, fill="#000000")

    # Shell scripts for system monitoring from here:
    # https://unix.stackexchange.com/questions/119126/command-to-display-memory-usage-disk-usage-and-cpu-load
    cmd = "hostname"
    hostname = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "hostname -I | cut -d' ' -f1"
    IP = "IP: " + subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "top -bn1 | grep load | awk '{printf \"CPU Load: %.2f\", $(NF-2)}'"
    CPU = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "free -m | awk 'NR==2{printf \"Mem: %s/%s MB  %.2f%%\", $3,$2,$3*100/$2 }'"
    MemUsage = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = 'df -h | awk \'$NF=="/"{printf "Disk: %d/%d GB  %s", $3,$2,$5}\''
    Disk = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "cat /sys/class/thermal/thermal_zone0/temp |  awk '{printf \"CPU Temp: %.1f C\", $(NF-0) / 1000}'"  # pylint: disable=line-too-long
    Temp = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "date +%Y%m%d-%H%M%S"
    TaD = subprocess.check_output(cmd, shell=True).decode("utf-8")
    cmd = "vcgencmd get_throttled"
    Throttled = subprocess.check_output(cmd, shell=True).decode("utf-8")
    out = Throttled.strip().strip("throttled=")
    display_message = "vcgencmd: " + out
    if out == "0x0":
        display_message = display_message + " No Warning"
    # convert hex to binary 
    out_int = int(out, 16)
    #display_message += "\nint-" + str(out_int)
    bit_nums = [[0, "Under_Voltage detected"],
                [1, "Arm frequency capped"],
                [2, "Currently throttled"],
                [3, "Soft temperature limit active"],
                [16, "Under-voltage has occurred"],
                [17, "Arm frequency capping has occurred"],
                [18, "Throttling has occurred"],
                [19, "Soft temperature limit has occurred"]]
    for x in range(0, len(bit_nums)):
        bit_num  = bit_nums[x][0]
        bit_text = bit_nums[x][1]
        if (out_int & ( 1 << bit_num )):
            display_message += "\n  - " + bit_text
    Throttled = display_message


    # Write four lines of text.
    y = top
#    draw.text((x, y), hostname, font=font, fill="#00FFFF")
#    y += font.getsize(hostname)[1]
#    draw.text((x, y), IP, font=font, fill="#FFFFFF")
#    y += font.getsize(IP)[1]
    draw.text((x, y), CPU, font=font, fill="#FFFF00")
    y += font.getsize(CPU)[1]
    draw.text((x, y), MemUsage, font=font, fill="#00FF00")
    y += font.getsize(MemUsage)[1]
    draw.text((x, y), Disk, font=font, fill="#0000FF")
    y += font.getsize(Disk)[1]
    draw.text((x, y), Temp, font=font, fill="#FF00FF")
    y += font.getsize(TaD)[1]
    draw.text((x,y), TaD, font=font, fill="#FF0000")
    y += font.getsize(Throttled)[1]
    draw.text((x,y), Throttled, font=font, fill="#FFFF00")
    # Display image.
    disp.image(image, rotation)
    time.sleep(5)
